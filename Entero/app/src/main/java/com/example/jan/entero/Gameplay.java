package com.example.jan.entero;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Intent;
import android.graphics.PointF;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.DragEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;


public class Gameplay extends Activity {
    ImageView img;
    String msg;
    MediaPlayer sound3;
    private android.widget.RelativeLayout.LayoutParams layoutParams;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gameplay);

        sound3 = MediaPlayer.create(Gameplay.this,R.raw.bgm_home_1);
        sound3.start();



        ImageView enemy1 = (ImageView) findViewById(R.id.enemy1);

        TranslateAnimation animation = new TranslateAnimation(0.0f, 400.0f,
                0.0f, 0.0f);
        animation.setDuration(5000);
        animation.setRepeatCount(20);
        animation.setRepeatMode(2);

        enemy1.startAnimation(animation);


        ImageView enemy4 = (ImageView) findViewById(R.id.enemy4);

        TranslateAnimation e4 = new TranslateAnimation(0.0f, 400.0f,
                0.0f, 0.0f);
        animation.setDuration(3000);
        animation.setRepeatCount(20);
        animation.setRepeatMode(2);

        enemy4.startAnimation(animation);

    }
    public void sendMessage5(View view){
        Intent intent = new Intent(this, Score.class);
        startActivity(intent);
    }
    public void sendMessage6(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_gameplay, menu);
        return true;
    }


    public void onPause() {
        super.onPause();
        sound3.pause();
    }

    public void onResume() {
        super.onResume();
        sound3.setLooping(true);
        sound3.start();
    }





    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
